(** Functions for parsing a generic CoNLL-U-like file *)

open Angstrom
open Extensions
open Parsing

open Sexplib.Conv
module Sexp = Sexplib.Sexp

module Dict = Map.Make(String)

let default_empty_field  = string "_" *> return ()
let default_field_sep    = tab *> return ()
let default_comment_sep  = return ()
let default_word_sep     = maybe hblank *> end_of_line
let default_sentence_end = end_of_line <|> end_of_input

let kv_comment =
  let* _ = maybe hblank *> string "#" *> maybe hblank in
  let* k = many_till (not_char '\n') (maybe hblank *> string "=") |> to_string in
  let* v = rest_of_line in
  return (k, v)

let simple_comment = maybe hblank *> string "#" *> rest_of_line

let default_comment =
  Option.some <$> kv_comment
  <|> (simple_comment *> return None)

module CoNLL = struct
  type parsed_field = Sexp.t

  type field_desc = {
    name : string;
    parse : parsed_field Angstrom.t;
    optional : bool;
  }

  let field name parse = { name; parse; optional = false }

  let opt f = { f with optional = true }

  let text_field name = field name (
      many_till any_char (peek_the_char '\t')
      |> to_atom
    )

  let parse_keyval =
    let* k = many_till any_char (string "=") |> to_string in
    let* v = many_till any_char (peek_one_of "|\t\n\r") |> to_string in
    return (k, v)

  let parse_dict = sep_by (string "|") parse_keyval

  let sexp_of_keyval (k,v) = Sexp.(List [Atom k; Atom v])
  let sexp_of_assoc assoc = Sexp.List (assoc |> List.map sexp_of_keyval)

  let dict_field name = field name (parse_dict |> map ~f:sexp_of_assoc)

  type parsed_word = parsed_field Dict.t

  let parse_field empty f =
    let to_pair s = (f.name, s) in
    let check_empty = if f.optional then maybe empty else return None in
    let* e = check_empty in
    match e with
    | Some _ -> return None
    | None -> f.parse |> map ~f:to_pair |> map ~f:Option.some

  let parse_word
      ?(separator = default_field_sep) ?(empty = default_empty_field)
      fields =
    List.map (parse_field empty) fields
    |> list_sep separator
    |> map ~f:Option.concat
    |> map ~f:List.to_seq
    |> map ~f:Dict.of_seq

  type comment = (string * string) option

  type parsed_sentence = {
    metadata : string Dict.t;
    words : parsed_word list;
  }

  type sentence_desc = {
    comment : comment Angstrom.t;
    comment_sep : unit Angstrom.t;
    word : parsed_word Angstrom.t;
    word_sep : unit Angstrom.t;
    stop : unit Angstrom.t;
  }

  let sentence ?(comment_sep = default_comment_sep)
      ?(word_sep = default_word_sep) ?(stop = default_sentence_end)
      ?(comment = default_comment) word =
    {
      comment; comment_sep; word; word_sep; stop
    }

  let parse_sentence d =
    let* comments = maybe blank *> many d.comment <* commit
                    <?> "(;_;) Couldn't parse comments" in
    let* _ = d.comment_sep *> commit
             <?> "(O_O) This should be the end of the comments" in
    let* words = sep_by d.word_sep d.word <* commit
                 <?> "(;_;) Couldn't parse words" in
    let* _ = d.stop <* commit
             <?> "(O_O) This should be the end of the sentence" in

    return { metadata = Dict.of_seq (List.to_seq (Option.concat comments));
             words }

  let rec parse_step sentence file_pos () =
    let eof = maybe blank *> end_of_input in
    match parse_in_file eof file_pos with
    | Ok _ -> Seq.Nil
    | _ -> match parse_in_file sentence file_pos with
      | Error e -> Seq.Cons (Error e, Seq.empty)
      | Ok o -> Seq.Cons (Ok o.value, parse_step sentence o.next)

  let parse sentence file = parse_step sentence (file_begin file)

  let sexp_of_word ?(set_name = Text.lower) word =
    let sexp_of_pair (k,v) = Sexp.(List [Atom (set_name k); v]) in
    Sexp.List (word |> Dict.to_seq |> List.of_seq |> List.map sexp_of_pair)

  let extract_sexp ?(set_name = Text.lower) desc word =
    let with_name name s = Sexp.(List [Atom (set_name name); s]) in

    let field_value f =
      if f.optional
      then (Dict.find_opt f.name word) |> Option.map (with_name f.name)
      else Some (with_name f.name (Dict.find f.name word))
    in

    try Some (Sexp.List (desc |> List.map field_value |> Option.concat))
    with Not_found -> None
end

module Default = struct
 type word = string Dict.t

 type sentence = {
   metadata : string Dict.t;
   words : word list;
 }

 let field_sep = default_field_sep
 let empty_field = default_empty_field
 let comment = default_comment
 let word_sep = default_word_sep
 let comment_sep = default_comment_sep
 let sentence_end = default_sentence_end

 let word_desc = [
   CoNLL.(opt (text_field "ID"));
   CoNLL.(opt (text_field "FORM"));
   CoNLL.(opt (text_field "LEMMA"));
   CoNLL.(opt (text_field "UPOS"));
   CoNLL.(opt (text_field "XPOS"));
   CoNLL.(opt (text_field "FEATS"));
   CoNLL.(opt (text_field "HEAD"));
   CoNLL.(opt (text_field "DEPREL"));
   CoNLL.(opt (text_field "DEPS"));
   CoNLL.(opt (text_field "MISC"));
 ]

 let word = CoNLL.parse_word word_desc

 let sentence_desc = CoNLL.sentence word

 let parse_sentence = CoNLL.parse_sentence sentence_desc

 type assoc_list = (string * string) list
 [@@deriving sexp]

 let sentence_of_conll_sentence (s : CoNLL.parsed_sentence) = {
   metadata = s.metadata;
   words = s.words |> List.map (Dict.map Sexp.to_string)
 }

 let sentence = parse_sentence |> map ~f:sentence_of_conll_sentence

 let parse = CoNLL.parse sentence
end
