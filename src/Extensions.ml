let (=) = Int.equal
let (<) a b = (Int.compare a b) = -1
let (>) a b = (Int.compare a b) =  1
let (<=) a b =
  let c = Int.compare a b in
  c = -1 || c = 0
let (>=) a b =
  let c = Int.compare a b in
  c = 0 || c = 1

let rec seq_range a b = fun () ->
  if a = b then Seq.Nil
  else Seq.Cons (a, seq_range (a+1) b)

module Array = struct
  include Stdlib.Array

  let get_opt arr i =
    try Some (Array.get arr i)
    with Invalid_argument _ -> None
end

module Char = struct
  include Stdlib.Char

  let concat cs = cs |> List.to_seq |> String.of_seq
end

module List = struct
  include Stdlib.List

  let is_empty l = match l with
    | [] -> true
    | _ -> false

  let range a b = seq_range a b |> of_seq
end

module Option = struct
  include Stdlib.Option

  let rec concat xs = match xs with
    | [] -> []
    | None :: t -> concat t
    | (Some h) :: t -> h :: concat t
end

module Seq = struct
  include Stdlib.Seq

  let rec exists p seq = match seq () with
    | Seq.Nil -> false
    | Seq.Cons (h,t) ->
      if p h
      then true
      else exists p t

  let rec for_all p seq = match seq () with
    | Seq.Nil -> true
    | Seq.Cons (h, t) ->
      if p h then for_all p t
      else false

  let range = seq_range
end
