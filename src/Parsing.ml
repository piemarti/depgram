open Angstrom
open Extensions
module Sexp = Sexplib.Sexp

type file_pos = in_channel * int

let file_begin f = (f, 0)

let line_at (ic, pos) =
  seek_in ic pos;
  try
    let line = input_line ic ^ "\n" in
    let new_pos = pos + String.length line in
    (`String line, (ic, new_pos))
  with End_of_file -> (`Eof, (ic, pos))

let feed_next_line partial file_pos =
  let (line, next) = line_at file_pos in
  let new_state = partial line in
  (new_state, next)

type 'a parse_result = {
  value : 'a;
  next : file_pos;
}

let rec get_result (state, file_pos) =
  let open Buffered in
  match state with
  | Partial partial -> feed_next_line partial file_pos |> get_result
  | Done (_, a) -> Ok {value = a; next = file_pos}
  | Fail (_, errs, e) -> Error (String.concat "\n" errs ^ "\n\n" ^ e)

let parse_in_file p file_pos =
  let state = Buffered.parse p in
  get_result (state, file_pos)

let peek_the_char c =
  let* c' = peek_char_fail in
  if Char.equal c c'
  then return c
  else fail (Printf.sprintf "char %C" c)

let one_of s =
  let* c = any_char in
  if String.contains s c
  then return c
  else fail ("One of " ^ String.escaped s)

let peek_one_of s =
  let* c = peek_char_fail in
  if String.contains s c
  then return c
  else fail ("Peek one of " ^ String.escaped s)

let to_string      p = p >>| Char.concat
let to_atom        p = p |> to_string |> map ~f:(fun s -> Sexp.Atom s)

let rest_of_line = many_till any_char (end_of_line <|> end_of_input) |> to_string

let (<||>) p q = (p *> return ()) <|> (q *> return ())

let maybe p    = option Option.none (p >>| Option.some)
let either p q = (p >>| Result.ok) <|> (q >>| Result.error)

let rec add_sep sep ps =
  match ps with
  | [] | _::[] -> ps
  | h::t -> (h <* sep) :: (add_sep sep t)

let list_sep sep ps = list (add_sep sep ps)

let tab        = char '\t'
let space      = char ' '
let whitespace = tab <|> space
let hblank     = many1 whitespace *> return ()
let blank      = many1 (whitespace <||> end_of_line) *> return ()
let digit      = one_of "0123456789"
