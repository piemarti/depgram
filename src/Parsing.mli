(** Useful functions for parsing *)

module Sexp = Sexplib.Sexp

type file_pos = in_channel * int
(** Points to a specific position in a file because operations on in_channel
    mutate it *)

val file_begin : in_channel -> file_pos
(** Points to the beginning of the file *)

type 'a parse_result = {
  value : 'a;       (** Result given by the parser *)
  next : file_pos; (** New position in the file *)
}

val parse_in_file : 'a Angstrom.t -> file_pos -> ('a parse_result, string) result
(** Converts an Angstrom parser to a file_pos parser *)

val peek_the_char : char -> char Angstrom.t
(** Check if next char is the given char without advancing the input *)

val one_of : string -> char Angstrom.t
(** Parses one of the chars in the given string *)

val peek_one_of : string -> char Angstrom.t
(** Parses one of the chars in the given string without advancing the input *)

val to_string : char list Angstrom.t -> string Angstrom.t

val to_atom : char list Angstrom.t -> Sexp.t Angstrom.t

val rest_of_line : string Angstrom.t
(** Parses the rest of the line until a new line or the end of file *)

val (<||>) : 'a Angstrom.t -> 'b Angstrom.t -> unit Angstrom.t
(** Alternative between two parsers of different types that returns nothing when
    it succeeds*)

val maybe : 'a Angstrom.t -> 'a option Angstrom.t
val either : 'a Angstrom.t -> 'b Angstrom.t -> ('a, 'b) result Angstrom.t

val add_sep : 's Angstrom.t -> 'a Angstrom.t list -> 'a Angstrom.t list
(** [add_sep sep ps] adds the separator [sep] to the inner elements of the list
    [ps] of parsers *)

val list_sep : 's Angstrom.t -> 'a Angstrom.t list -> 'a list Angstrom.t
(** [list_sep sep ps] parses with a list [ps] of parsers separated by [sep] *)

val tab : char Angstrom.t
val space : char Angstrom.t
val whitespace : char Angstrom.t
val hblank : unit Angstrom.t
val blank : unit Angstrom.t
val digit : char Angstrom.t
