open Angstrom
open Extensions
open Parsing
open Sexplib.Conv

module Dict = CoNLL.Dict
module CoNLL = CoNLL.CoNLL
module Sexp = Sexplib.Sexp

module Id = struct
  type t = At of int
         | After of int*int
         | Between of int*int
  [@@deriving sexp, show, eq]

  let compare i1 i2 = match i1, i2 with
    | At i, At j                   -> Int.compare i j
    | At i, After (j,_)            -> if i <= j then -1 else 1
    | At i, Between (j,_)          -> if i < j then -1 else 1
    | After (i,_), At j            -> if i < j then -1 else 1
    | After (i,k), After (j,l)     -> if i = j then Int.compare k l
                                              else Int.compare i j
    | After (i,_), Between (j,_)   -> if i < j then -1 else 1
    | Between (i,_), At j          -> if i <= j then -1 else 1
    | Between (i,_), After (j,_)   -> if i <= j then -1 else 1
    | Between (i,_), Between (j,_) -> Int.compare i j

  let parse_between =
    let* a = many1 digit <* string "-" |> to_string in
    let* b = many1 digit |> to_string in
    return Sexp.(List [Atom "Between"; Atom a; Atom b])

  let parse_after =
    let* a = many1 digit <* string "." |> to_string in
    let* b = many1 digit |> to_string in
    return Sexp.(List [Atom "After"; Atom a; Atom b])

  let parse_at =
    let* a = many1 digit |> to_string in
    return Sexp.(List [Atom "At"; Atom a])

  let parse = parse_between <|> parse_after <|> parse_at
              <?> "Id"

  let field name = CoNLL.field name parse

  let to_string id = match id with
    | At a -> (Int.to_string a)
    | After (a,b) -> (Int.to_string a) ^ "." ^ (Int.to_string b)
    | Between (a,b) -> (Int.to_string a) ^ "-" ^ (Int.to_string b)
end

module PoS = struct
  type t = ADJ   (** adjective *)
         | ADP   (** adposition *)
         | ADV   (** adverb *)
         | AUX   (** auxiliary *)
         | CCONJ (** coordinating conjunction *)
         | DET   (** determiner *)
         | INTJ  (** interjection *)
         | NOUN  (** noun *)
         | NUM   (** numeral *)
         | PART  (** particle *)
         | PRON  (** pronoun *)
         | PROPN (** proper noun *)
         | PUNCT (** punctuation *)
         | SCONJ (** subordinating conjunction *)
         | SYM   (** symbol *)
         | VERB  (** verb *)
         | X     (** other *)
  [@@deriving sexp, show, eq]

  let parse =
    let* pos = choice (List.map string [
      "ADJ"; "ADP"; "ADV"; "AUX"; "CCONJ"; "DET"; "INTJ"; "NOUN"; "NUM"; "PART";
      "PRON"; "PROPN"; "PUNCT"; "SCONJ"; "SYM"; "VERB"; "X"
    ]) <?> "part of speech"
    in
    return (Sexp.Atom pos)
              <?> "PoS"

  let field name = CoNLL.field name parse

  let to_string pos = match (sexp_of_t pos) with
    | Atom s -> s
    | _ -> failwith "(;_;) PoS.sexp_of_t is wrong (PoS.to_string)"
end

module Deprel = struct
  type ud = Acl        (** clausal modifier of noun (adnominal clause) *)
          | Advcl      (** adverbial clause modifier *)
          | Advmod     (** adverbial modifier *)
          | Amod       (** adjectival modifier *)
          | Appos      (** appositional modifier *)
          | Aux        (** auxiliary *)
          | Case       (** case marking *)
          | Cc         (** coordinating conjunction *)
          | Ccomp      (** clausal complement *)
          | Clf        (** classifier *)
          | Compound   (** compound *)
          | Conj       (** conjunct *)
          | Cop        (** copula *)
          | Csubj      (** clausal subject *)
          | Dep        (** unspecified dependency *)
          | Det        (** determiner *)
          | Discourse  (** discourse element *)
          | Dislocated (** dislocated elements *)
          | Expl       (** expletive *)
          | Fixed      (** fixed multiword expression *)
          | Flat       (** flat multiword expression *)
          | Goeswith   (** goes with *)
          | Iobj       (** indirect object *)
          | List       (** list *)
          | Mark       (** marker *)
          | Nmod       (** nominal modifier *)
          | Nsubj      (** nominal subject *)
          | Nummod     (** numeric modifier *)
          | Obj        (** object *)
          | Obl        (** oblique nominal *)
          | Orphan     (** orphan *)
          | Parataxis  (** parataxis *)
          | Punct      (** punctuation *)
          | Ref        (** enhanced relation *)
          | Reparandum (** overridden disfluency *)
          | Root       (** root *)
          | Vocative   (** vocative *)
          | Xcomp      (** open clausal complement *)
  [@@deriving sexp, show, eq]

  type t = {
    dtype : ud;              (** Universal Dependency relation *)
    subtype : string option; (** Language-specific subtype *)
  }
  [@@deriving sexp, show, eq]

  let parse =
    let* dtype = choice (List.map string [
        "acl"; "advcl"; "advmod"; "amod"; "appos"; "aux"; "case"; "ccomp"; "cc";
        "clf"; "compound"; "conj"; "cop"; "csubj"; "dep"; "det"; "discourse";
        "dislocated"; "expl"; "fixed"; "flat"; "goeswith"; "iobj"; "list";
        "mark"; "nmod"; "nsubj"; "nummod"; "obj"; "obl"; "orphan"; "parataxis";
        "punct"; "ref"; "reparandum"; "root"; "vocative"; "xcomp"
      ]) <?> "(T_T) couldn't parse universal deprel"
    in
    let* subtype = maybe (string ":" *> many_till any_char
                            (peek_the_char '\t' <||> end_of_input))
                   |> map ~f:(Option.map Char.concat)
    in

    return Sexp.(List [ List [Atom "dtype"; Atom dtype];
                        List [Atom "subtype";
                              sexp_of_option (fun s -> Atom s) subtype];
                      ])
              <?> "Deprel"

  let field name = CoNLL.field name parse

  let ud_to_string ud = match sexp_of_ud ud with
    | Atom s -> String.lowercase_ascii s
    | _ -> failwith "(;_;) Deprel.sexp_of_ud is wrong (Deprel.to_string)"

  let opt_to_string opt =
    opt |> Option.map (fun s -> ":"^s) |> Option.value ~default:""

  let to_string deprel =
    ud_to_string deprel.dtype
    ^ opt_to_string deprel.subtype
end

module EnhancedDeprel = struct
  type t = {
    dtype : Deprel.ud;              (** Universal Dependency relation *)
    subtype : string option;        (** Language-specific subtype *)
    case_dependent : string option; [@default None] (** Case information *)
    case : string option;           [@default None] (** Case information *)
  }
  [@@deriving sexp, show, eq]

  let parse =
    let* deprel = Deprel.parse in
    let* case_dep = maybe (string ":" *> many_till any_char
                             (peek_the_char '\t' <||> end_of_input))
                   |> map ~f:(Option.map Char.concat)
    in
    let* case = maybe (string ":" *> many_till any_char
                         (peek_the_char '\t' <||> end_of_input))
                   |> map ~f:(Option.map Char.concat)
    in

    match deprel with
    | Sexp.Atom _ -> fail "(T_T) deprel didn't work!"
    | Sexp.List l ->
      return (Sexp.List (List.append l Sexp.[
          List [Atom "case_dependent";
                sexp_of_option (fun s -> Atom s) case_dep];
          List [Atom "case";
                sexp_of_option (fun s -> Atom s) case];
        ]))
      <?> "EnhancedDeprel"

  let field name = CoNLL.field name parse

  let to_string deprel =
    Deprel.ud_to_string deprel.dtype
    ^ Deprel.opt_to_string deprel.subtype
    ^ Deprel.opt_to_string deprel.case_dependent
    ^ Deprel.opt_to_string deprel.case
end

module Deps = struct
  type t = (Id.t * EnhancedDeprel.t) list
  [@@deriving sexp, show, eq]

  let parse_pair =
    let* id = Id.parse <* char ':' in
    let* deprel = Deprel.parse in
    return Sexp.(List [id; deprel])

  let parse =
    let* deps = sep_by1 (string "|") parse_pair in
    return Sexp.(List deps) <?> "Deps"

  let field name = CoNLL.field name parse

  let pair_to_string (id, deprel) =
    (Id.to_string id) ^ ":" ^ (EnhancedDeprel.to_string deprel)

  let to_string deps = List.map pair_to_string deps |> String.concat "|"
end

module StringDict = struct
  type t = string Dict.t
  type assoc = (string * string) list [@@deriving sexp]

  let t_of_sexp sexp = sexp |> assoc_of_sexp |> List.to_seq |> Dict.of_seq
  let sexp_of_t t = t |> Dict.to_seq |> List.of_seq |> sexp_of_assoc

  let kv_to_string (k,v) = k ^ "=" ^ v
  let to_string t = t |> Dict.to_seq
                    |> Seq.map kv_to_string
                    |> List.of_seq
                    |> String.concat "|"
end

module Word = struct
  type t = {
    id : Id.t;
    form : string;
    lemma : string;
    upos : PoS.t;
    xpos : string                         [@default ""];
    feats : StringDict.t                  [@default Dict.empty];
    head : Id.t;
    deprel : Deprel.t;
    deps : (Id.t * EnhancedDeprel.t) list [@default []];
    misc : StringDict.t                   [@default Dict.empty];
  } [@@deriving sexp]

  let is_enhanced w = match w.id with
    | After _ -> true
    | _ -> false

  let desc = CoNLL.([
      Id.field "ID";
      text_field "FORM";
      text_field "LEMMA";
      PoS.field "UPOS";
      opt (text_field "XPOS");
      opt (dict_field "FEATS");
      Id.field "HEAD";
      Deprel.field "DEPREL";
      opt (Deps.field "DEPS");
      opt (dict_field "MISC");
    ])

  let parse = CoNLL.parse_word desc <?> "Word"

  let to_string w = String.concat "\t" [
      Id.to_string w.id;
      w.form; w.lemma;
      PoS.to_string w.upos;
      w.xpos;
      StringDict.to_string w.feats;
      Id.to_string w.head;
      Deprel.to_string w.deprel;
      Deps.to_string w.deps;
      StringDict.to_string w.misc;
    ]
end

module ExtraWord = struct
  type t = {
    id : Id.t;
    form : string;
    lemma : string;
    upos : PoS.t;
    xpos : string                         [@default ""];
    feats : StringDict.t                  [@default Dict.empty];
    deps : (Id.t * EnhancedDeprel.t) list [@default []];
    misc : StringDict.t                   [@default Dict.empty];
  } [@@deriving sexp]

  let empty_field name =
    CoNLL.(opt (field name (fail ("(ò_ó) The field "
                                  ^ name ^ " should be empty"))))

  let desc = CoNLL.([
      Id.field "ID";
      text_field "FORM";
      text_field "LEMMA";
      PoS.field "UPOS";
      opt (text_field "XPOS");
      opt (dict_field "FEATS");
      empty_field "HEAD";
      empty_field "DEPREL";
      opt (Deps.field "DEPS");
      opt (dict_field "MISC");
    ])

  let parse = CoNLL.parse_word desc <?> "ExtraWord"

  let to_string w = String.concat "\t" [
      Id.to_string w.id;
      w.form; w.lemma;
      PoS.to_string w.upos;
      w.xpos;
      StringDict.to_string w.feats;
      "_"; "_";
      Deps.to_string w.deps;
      StringDict.to_string w.misc;
    ]
end

module MultiWord = struct
  type t = {
    id : Id.t;
    form : string;
    misc : StringDict.t [@default Dict.empty];
  } [@@deriving sexp]

  let empty_field name =
    CoNLL.(opt (field name (fail ("(ò_ó) The field "
                                  ^ name ^ " should be empty"))))

  let desc = CoNLL.([
      Id.field "ID";
      text_field "FORM";
      opt (text_field "LEMMA");
      empty_field "UPOS";
      empty_field "XPOS";
      empty_field "FEATS";
      empty_field "HEAD";
      empty_field "DEPREL";
      empty_field "DEPS";
      opt (dict_field "MISC");
    ])

  let parse = CoNLL.parse_word desc <?> "MultiWord"

  let to_string w = String.concat "\t" [
      Id.to_string w.id; w.form;
      "_"; "_"; "_"; "_"; "_"; "_"; "_";
      StringDict.to_string w.misc;
    ]
end

module Sentence = struct
  type t = {
    metadata : string Dict.t;
    words : Word.t list;
    extrawords : ExtraWord.t list;
    multiwords : MultiWord.t list;
  }

  let to_dependency sentence =
    let open Word in

    let linear_order =
      List.sort (fun u v -> Id.compare u.id v.id) sentence.words in

    let dep_of w =
      match w.id with
      | At i -> (match w.head with
          | At j -> Some (j, i, w.deprel)
          | _ -> None)
      | _ -> None
    in

    let dependencies = List.filter_map dep_of sentence.words in

    Dependency.of_list linear_order dependencies

  let parse_word = Word.parse <|> ExtraWord.parse <|> MultiWord.parse

  type any_word = Word of Word.t
                | ExtraWord of ExtraWord.t
                | MultiWord of MultiWord.t

  let extract_word w =
    match CoNLL.extract_sexp Word.desc w with
    | Some w -> Word (Word.t_of_sexp w)
    | None ->
      match CoNLL.extract_sexp ExtraWord.desc w with
      | Some w -> ExtraWord (ExtraWord.t_of_sexp w)
      | None ->
        match CoNLL.extract_sexp MultiWord.desc w with
        | Some w -> MultiWord (MultiWord.t_of_sexp w)
        | None ->
          raise (Invalid_argument
                   "word_or_multiword: w is neither a word nor a multiword")

  let word_opt aw = match aw with | Word w -> Some w | _ -> None
  let extra_word_opt aw = match aw with | ExtraWord ew -> Some ew | _ -> None
  let multi_word_opt aw = match aw with | MultiWord mw -> Some mw | _ -> None

  let make (sent : CoNLL.parsed_sentence) =
    let wordlist = sent.words |> List.map extract_word in
    let words =
      wordlist |> List.filter_map word_opt in
    let extrawords =
      wordlist |> List.filter_map extra_word_opt in
    let multiwords =
      wordlist |> List.filter_map multi_word_opt in
    {
      metadata = sent.metadata;
      words;
      extrawords;
      multiwords;
    }

  let parse = CoNLL.parse_sentence (CoNLL.sentence parse_word)
            |> map ~f:make
end

let parse = CoNLL.parse Sentence.parse
