(** Functions for parsing a generic CoNLL-U-like file *)

module Sexp = Sexplib.Sexp
module Dict : Map.S with type key = string

(** Utilities to build a parser for a CoNLL-U-like file *)
module CoNLL : sig
  type parsed_field = Sexp.t
  (** A field in a CoNLL word line *)

  type field_desc = {
    name : string;                   (** column name *)
    parse : parsed_field Angstrom.t; (** how to parse the field *)
    optional : bool;                 (** true if the field is optional *)
  }
  (** Description of how to parse a field *)

  val field : string -> Sexp.t Angstrom.t -> field_desc
  (** [field name p] create a field_desc with name [name] parsed with [p] where
      optional is false *)

  val opt : field_desc -> field_desc
  (** [opt column] sets optional to true *)

  val text_field : string -> field_desc
  (** Parses the string of all characters until next tab *)

  val dict_field : string -> field_desc
  (** Parses a dictionary of the form "k1=v1|k2=v2|…"*)

  type parsed_word = parsed_field Dict.t
  (** A CoNLL word line (mapping from column name to value) *)

  val parse_word : ?separator : unit Angstrom.t -> ?empty: unit Angstrom.t
    -> field_desc list -> parsed_word Angstrom.t
  (** [word_parser sep empty fields] parses a word with the fields described by
      [fields] separated by [sep] (tab by default), where [empty] parses an empty
      column ("_" by default) *)

  type comment = (string * string) option
  (** A CoNLL comment as a key-value pair *)

  type parsed_sentence = {
    metadata : string Dict.t; (** Metadata from the comments *)
    words : parsed_word list; (** List of words *)
  }
  (** A CoNLL sentence *)

  type sentence_desc = {
    comment : comment Angstrom.t;  (** How to parse a comment *)
    comment_sep : unit Angstrom.t; (** Separator between comments and words *)
    word : parsed_word Angstrom.t; (** How to parse a word *)
    word_sep : unit Angstrom.t;    (** Separator between words *)
    stop : unit Angstrom.t;        (** End of a sentence *)
  }
  (** Description of how to parse a sentence *)

  val sentence : ?comment_sep : unit Angstrom.t -> ?word_sep : unit Angstrom.t
    -> ?stop : unit Angstrom.t -> ?comment : comment Angstrom.t
    -> parsed_word Angstrom.t -> sentence_desc
  (** Constructs a sentence_desc where:
      - comment_sep defaults to Default.comment_sep
      - word_sep defaults to Default.word_sep
      - stop defaults to Default.sentence_end
      - comment defaults to Default.comment *)

  val parse_sentence : sentence_desc -> parsed_sentence Angstrom.t
  (** Parse a sentence according to the given description *)

  val parse : 'sentence Angstrom.t -> in_channel
    -> ('sentence, string) result Seq.t
  (** Uses a sentence parser to parse a whole file *)

  val sexp_of_word : ?set_name : (string -> string) -> parsed_word -> Sexp.t
  (** [sexp_of_word set_name w] returns the word as an S-expression where
      set_name modifies the field names (by default it's Text.lower) *)

  val extract_sexp : ?set_name : (string -> string)
    -> field_desc list -> parsed_word -> Sexp.t option
    (** [extract_sexp set_name fields w] extracts the fields [fields] from [w]
        into an S-expresion if all fields required are present; where [set_name]
        sets the column name into the s-expression field name (by default it's
        Text.lower) *)
end

(** Default parsers for CoNLL-U files *)
module Default : sig

  type word = string Dict.t
  (** A word is a mapping from column name to its value as string *)

  type sentence = {
    metadata : string Dict.t;
    words : word list;
  }
  (** Sentence with default word types *)

  val field_sep : unit Angstrom.t
  (** Parses a tab *)

  val empty_field : unit Angstrom.t
  (** Parses an underscore *)

  val word_desc : CoNLL.field_desc list
  (** columns ID, FORM, LEMMA, UPOS, XPOS, FEATS, HEAD, DEPREL, DEPS, MISC as
      optional text fields *)

  val word : CoNLL.parsed_word Angstrom.t
  (** Parses a word with word_desc *)

  val comment : CoNLL.comment Angstrom.t
  (** Parse a comment of the form "# key = value" *)

  val word_sep : unit Angstrom.t
  (** Parses a new line *)

  val comment_sep : unit Angstrom.t
  (** Parses a new line *)

  val sentence_end : unit Angstrom.t
  (** Parses at least two new lines *)

  val parse_sentence : CoNLL.parsed_sentence Angstrom.t
  (** Parses a sentence in the Sexp-based type *)

  val sentence : sentence Angstrom.t
  (** Parses a sentence and construct the default sentence type *)

  val parse : in_channel -> (sentence, string) result Seq.t
  (** Parses an entire CoNLL file *)
end
