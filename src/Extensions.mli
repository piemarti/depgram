(** Custom library extensions *)

(** Get rid of polymorphic comparison (greatly improves performance!): *)
val (=) : int -> int -> bool
val (<) : int -> int -> bool
val (>) : int -> int -> bool
val (<=) : int -> int -> bool
val (>=) : int -> int -> bool

module Array : sig
  include module type of Stdlib.Array
  val get_opt : 'a t -> int -> 'a option
end

module Char : sig
  include module type of Stdlib.Char
  val concat : t list -> string
end

module List : sig
  include module type of Stdlib.List
  val is_empty : 'a list -> bool
  val range : int -> int -> int t
end

module Option : sig
  include module type of Stdlib.Option
  val concat : 'a t list -> 'a list
end

module Seq : sig
  include module type of Stdlib.Seq

  val exists : ('a -> bool) -> 'a t -> bool
  val for_all : ('a -> bool) -> 'a t -> bool
  val range : int -> int -> int t
end
