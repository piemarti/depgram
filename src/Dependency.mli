(** Dependency structure *)

type ('word, 'dep) t
(** The dependency structure of a sentence *)

type ('word, 'dep) node
(** A node in the dependency structure *)

val of_list : 'word list -> (int * int * 'dep) list -> ('word, 'dep) t
(** Construct a dependency structure from a list of words (in linear order) and
    a list of (head, dependent, relation) tuples *)

val to_list : ('word, 'dep) t -> ('word list * (int * int * 'dep) list)
(** Returns the list of words in linear order together with the list of
    dependency tuples *)

val map_words : ('word1 -> 'word2) -> ('word1, 'dep) t -> ('word2, 'dep) t
(** Map over words *)

val map_deps : ('dep1 -> 'dep2) -> ('word, 'dep1) t -> ('word, 'dep2) t
(** Map over dependencies *)

val nth : ('word, 'dep) t -> int -> ('word, 'dep) node option
(** Returns the nth node in linear order *)

val nodes : ('word, 'dep) t -> ('word, 'dep) node Seq.t
(** Return the nodes in linear order *)

val word : ('word, 'dep) node -> 'word
(** The word corresponding to the node *)

val heads : ('word, 'dep) node -> ('word, 'dep) node list
(** The heads of the word *)

val head : ('word, 'dep) node -> ('word, 'dep) node option
(** The first head in the list, if there are any *)

val deprels : ('word, 'dep) node -> 'dep list
(** The relations from the current node to its head *)

val deprel : ('word, 'dep) node -> 'dep option
(** The relations from the current node to its first head *)

val index : ('word, 'dep) node -> int
(** The index of the word in the sentence *)

val sentence : ('word, 'dep) node -> ('word, 'dep) t
(** The dependency structure the node is part of *)

val next : ('word, 'dep) node -> ('word, 'dep) node option
(** Next word in linear order *)

val prev : ('word, 'dep) node -> ('word, 'dep) node option
(** Previous word in linear order *)

val equal_node : ('word, 'dep) node -> ('word, 'dep) node -> bool
(** Returns true if the nodes are equal *)

val compare_node : ('word, 'dep) node -> ('word, 'dep) node -> int
(** Compare nodes *)

val is_root : ('word, 'dep) node -> bool
(** Returns true if the node is the root of the sentence *)

val children : ('word, 'dep) node -> ('word, 'dep) node Seq.t
(** Words that have the node's word as their head *)

val left_children : ('word, 'dep) node -> ('word, 'dep) node Seq.t
(** Children on the left in linear order *)

val right_children : ('word, 'dep) node -> ('word, 'dep) node Seq.t
(** Children on the right in linear order *)

val add_dep : ('word, 'dep) node -> ('word, 'dep) node -> 'dep -> ('word, 'dep) t
(** add_dep [head] [dep] [deprel] Add a dependency from head to dep using
    deprel *)

val governors : ('word, 'dep) node -> ('word, 'dep) node Seq.t
(** Sequence of the word's heads until the root *)

val governs : ('word, 'dep) node -> ('word, 'dep) node -> bool
(** Returns true if the first word governs the second *)

val projection : ('word, 'dep) node -> ('word, 'dep) node Seq.t
(** Sequence of words that are dominated by the node's word *)

val is_continuous : ('word, 'dep) node Seq.t -> bool
(** Returns true if the sequence of nodes is continuous wrt word order*)

val is_dep_projective : ('word, 'dep) node -> bool
(** Returns true if the dependency with that node as dependent is projective *)

val is_projective : ('word, 'dep) t -> bool
(** Returns true if the sentence is projective *)
