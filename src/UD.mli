(** Universal Dependencies types and parsers *)

module Dict = CoNLL.Dict
module CoNLL = CoNLL.CoNLL
module Sexp = Sexplib.Sexp

module Id : sig
  type t = At of int          (** The position of a token *)
         | After of int*int   (** A node added for the enhanced representation *)
         | Between of int*int (** A range of positions (for multi-word tokens) *)
  [@@deriving sexp, show, eq]

  val compare : t -> t -> int
  (** [compare i j] returns -1 if i < j, 0 if i = j and 1 if i > j. *)

  val parse : CoNLL.parsed_field Angstrom.t
  val field : string -> CoNLL.field_desc
  val to_string : t -> string
end

module PoS : sig
  type t = ADJ   (** adjective *)
         | ADP   (** adposition *)
         | ADV   (** adverb *)
         | AUX   (** auxiliary *)
         | CCONJ (** coordinating conjunction *)
         | DET   (** determiner *)
         | INTJ  (** interjection *)
         | NOUN  (** noun *)
         | NUM   (** numeral *)
         | PART  (** particle *)
         | PRON  (** pronoun *)
         | PROPN (** proper noun *)
         | PUNCT (** punctuation *)
         | SCONJ (** subordinating conjunction *)
         | SYM   (** symbol *)
         | VERB  (** verb *)
         | X     (** other *)
  [@@deriving sexp, show, eq]

  val parse : CoNLL.parsed_field Angstrom.t
  val field : string -> CoNLL.field_desc
  val to_string : t -> string
end

module Deprel : sig
  type ud = Acl        (** clausal modifier of noun (adnominal clause) *)
          | Advcl      (** adverbial clause modifier *)
          | Advmod     (** adverbial modifier *)
          | Amod       (** adjectival modifier *)
          | Appos      (** appositional modifier *)
          | Aux        (** auxiliary *)
          | Case       (** case marking *)
          | Cc         (** coordinating conjunction *)
          | Ccomp      (** clausal complement *)
          | Clf        (** classifier *)
          | Compound   (** compound *)
          | Conj       (** conjunct *)
          | Cop        (** copula *)
          | Csubj      (** clausal subject *)
          | Dep        (** unspecified dependency *)
          | Det        (** determiner *)
          | Discourse  (** discourse element *)
          | Dislocated (** dislocated elements *)
          | Expl       (** expletive *)
          | Fixed      (** fixed multiword expression *)
          | Flat       (** flat multiword expression *)
          | Goeswith   (** goes with *)
          | Iobj       (** indirect object *)
          | List       (** list *)
          | Mark       (** marker *)
          | Nmod       (** nominal modifier *)
          | Nsubj      (** nominal subject *)
          | Nummod     (** numeric modifier *)
          | Obj        (** object *)
          | Obl        (** oblique nominal *)
          | Orphan     (** orphan *)
          | Parataxis  (** parataxis *)
          | Punct      (** punctuation *)
          | Ref        (** enhanced relation *)
          | Reparandum (** overridden disfluency *)
          | Root       (** root *)
          | Vocative   (** vocative *)
          | Xcomp      (** open clausal complement *)
  [@@deriving sexp, show, eq]

  type t = {
    dtype : ud;              (** Universal Dependency relation *)
    subtype : string option; (** Language-specific subtype *)
  }
  [@@deriving sexp, show, eq]

  val parse : CoNLL.parsed_field Angstrom.t
  val field : string -> CoNLL.field_desc
  val to_string : t -> string
end

module EnhancedDeprel : sig
  type t = {
    dtype : Deprel.ud;              (** Universal Dependency relation *)
    subtype : string option;        (** Language-specific subtype *)
    case_dependent : string option; (** Case information *)
    case : string option;           (** Case information *)
  }
  [@@deriving sexp, show, eq]

  val parse : CoNLL.parsed_field Angstrom.t
  val field : string -> CoNLL.field_desc
  val to_string : t -> string
end

module Deps : sig
  type t = (Id.t * EnhancedDeprel.t) list
  [@@deriving sexp, show, eq]

  val parse : CoNLL.parsed_field Angstrom.t
  val field : string -> CoNLL.field_desc
  val to_string : t -> string
end

(** A word in a UD sentence *)
module Word : sig
  type t = {
    id : Id.t;
    form : string;
    lemma : string;
    upos : PoS.t;
    xpos : string                         [@default ""];
    feats : string Dict.t                 [@default Extensions.Dict.empty];
    head : Id.t;
    deprel : Deprel.t;
    deps : (Id.t * EnhancedDeprel.t) list [@default []];
    misc : string Dict.t                  [@default Extensions.Dict.empty];
  } [@@deriving sexp]

  val is_enhanced : t -> bool

  val desc : CoNLL.field_desc list
  val parse : CoNLL.parsed_word Angstrom.t
  val to_string : t -> string
end

(** An extra word (for enhanced dependencies) in a UD sentence *)
module ExtraWord : sig
  type t = {
    id : Id.t;
    form : string;
    lemma : string;
    upos : PoS.t;
    xpos : string                         [@default ""];
    feats : string Dict.t                 [@default Extensions.Dict.empty];
    deps : (Id.t * EnhancedDeprel.t) list [@default []];
    misc : string Dict.t                  [@default Extensions.Dict.empty];
  } [@@deriving sexp]

  val desc : CoNLL.field_desc list
  val parse : CoNLL.parsed_word Angstrom.t
  val to_string : t -> string
end

(** A word that spans several lemma in a UD sentence *)
module MultiWord : sig
  type t = {
    id : Id.t;
    form : string;
    misc : string Dict.t [@default Dict.empty];
  } [@@deriving sexp]

  val desc : CoNLL.field_desc list
  val parse : CoNLL.parsed_word Angstrom.t
  val to_string : t -> string
end

(** A CoNLL-U sentence *)
module Sentence : sig
  type t = {
    metadata : string Dict.t;
    words : Word.t list;
    extrawords : ExtraWord.t list;
    multiwords : MultiWord.t list;
  }

  val to_dependency : t -> (Word.t, Deprel.t) Dependency.t
  (** Returns the dependency structure of the sentence *)

  type any_word = Word of Word.t
                | ExtraWord of ExtraWord.t
                | MultiWord of MultiWord.t

  val parse_word : CoNLL.parsed_word Angstrom.t
  val extract_word : CoNLL.parsed_word -> any_word
  val make : CoNLL.parsed_sentence -> t
  val parse : t Angstrom.t
end

val parse : in_channel -> (Sentence.t, string) result Seq.t
(** Parse a CoNLL-U file into a sequence of UD sentences *)
