open Extensions

type ('word, 'dep) t = {
  words : 'word array;
  heads : (int * 'dep) list array;
}

type ('word, 'dep) node = {
  from: ('word, 'dep) t;
  at: int;
}

let of_list ws deps =
  let words = Array.of_list ws in

  let id_to_int id = if id <= 0 then None else Some (id - 1) in

  let deps_of i =
    deps |> List.filter
      (fun (_,d,_) -> Option.equal (=) (id_to_int d) (Some i))
  in

  let head (h,_d,rel) = Option.bind (id_to_int h) (fun i -> Some (i, rel)) in
  let heads i         = deps_of i |> List.map head |> Option.concat in

  {
    words;
    heads = (Seq.range 0 (Array.length words))
            |> Seq.map heads |> List.of_seq |> Array.of_list;
  }

let to_list ds =
  let full_dep i (j, rel) = (j, i, rel) in
  let full_deps i xs = List.map (full_dep i) xs in
  (ds.words |> Array.to_list,
   ds.heads |> Array.to_list |> List.mapi full_deps |> List.concat
  )

let map_words f ds =
  {
    words = Array.map f ds.words;
    heads = ds.heads;
  }

let map_deps f ds = {
  words = ds.words;
  heads = Array.map (List.map (fun (h, rel) -> (h, f rel))) ds.heads;
}

let nth ds n = Array.get_opt ds.words n
               |> Option.map (fun _ -> {from = ds; at = n})

let nodes ds =
  Seq.range 0 (Array.length ds.words)
  |> Seq.map (fun i -> {from = ds; at = i})

let word node = Array.get node.from.words node.at

let heads node =
  Array.get node.from.heads node.at
  |> List.map (fun (h,_) -> {node with at = h})

let head node = List.nth_opt (heads node) 0

let deprels node =
  Array.get node.from.heads node.at
  |> List.map (fun (_,rel) -> rel)

let deprel node = List.nth_opt (deprels node) 0

let index node = node.at

let sentence node = node.from

let next node = nth node.from (node.at + 1)
let prev node = nth node.from (node.at - 1)

let equal_node n1 n2 = n1.from == n2.from && Int.equal n1.at n2.at

let compare_node n1 n2 =
  if n1.from != n2.from then 0
  else Int.compare n1.at n2.at

let is_root node =
  match Array.get node.from.heads node.at with
  | [] -> true
  | _  -> false

let children node =
  let is_child_of node child = List.exists (equal_node node) (heads child) in
  Seq.filter (is_child_of node) (nodes node.from)

let left_children node =
  Seq.filter (fun n -> n.at < node.at) (children node)

let right_children node =
  Seq.filter (fun n -> n.at > node.at) (children node)

let add_dep h d rel =
  let new_deps = Array.copy h.from.heads in
  Array.set new_deps d.at (List.append (Array.get new_deps d.at) [(h.at, rel)]);
  {
    words = d.from.words;
    heads = new_deps;
  }

let rec governors node =
  fun () -> match head node with
    | None -> Seq.Cons(node, Seq.empty)
    | Some h -> Seq.Cons(node, governors h)

let governs g d = governors d |> Seq.exists (equal_node g)

let projection node = Seq.filter (governs node) (nodes node.from)

let is_continuous seq =
  let indices = Seq.map index seq |> List.of_seq
                |> List.sort_uniq Int.compare
  in
  if List.is_empty indices
  then true
  else
    let len = List.length indices in
    let first = List.hd indices in
    let last = List.nth indices (len-1) in
    first + len - 1 = last

let is_dep_projective node =
  match head node with
  | None -> true
  | Some h -> is_continuous (projection h)

let is_projective ds = Seq.for_all is_dep_projective (nodes ds)
